const { google } = require('googleapis');
const auth = require('../auth.json');

const DataHeader = {
  INFO: {
    CALLSIGN: 'callsign',	
    DISCORD: 'discord',
    TEAMSPEAK: 'teamspeak',
    STEAMID: 'steamid'
  },
  PROGRESSION: {
    JOIN_DATE: 'join-date',
    RASP_DATE: 'rasp-date',
    RANK: 'rank',
    CURRENT_STATUS: 'status'
  },
  ATTENDANCE: {
    ON_LEAVE: 'on-leave',	
    LEAVE_RETURN_DATE: 'leave-return-date',	
    FTX_ATTENDANCE: 'ftx-count',	
    MSO_ATTENDANCE: 'mso-count',
  },
  QUAL: {	
    QUAL_AIRBORNE: 'q-airborne',
    QUAL_MARKSMAN: 'q-marksman',
    QUAL_MACHINEGUN: 'q-machinegun',	
    QUAL_EXPLOSIVES: 'q-explosives',	
    QUAL_DIVE: 'q-dive',
    QUAL_AIR_CAS: 'q-aircas',
    QUAL_AIR_TRNS: 'q-airtrns',
    QUAL_AIR_FIXED: 'q-airfixed',	
    QUAL_AIR_UAV: 'q-airuav'
  }
}

const client = new google.auth.JWT(
  auth.client_email,
  null,
  auth.private_key,
  ['https://www.googleapis.com/auth/spreadsheets']
);

client.authorize(function (err, tokens) {
  if (err) {
    console.log(`Sheets Error: ${err}`);
    return
  } else {
    console.log(`Sheets Connected! `);
    gsrun(client);
  }
})

/**
 * 
 * @param {JWT} authClient
 */
async function gsrun(authClient) {
  const gsapi = google.sheets({ version: 'v4', auth: authClient })

  const options = {
    spreadsheetId: '15x0RtUCxaiOiIrl1xaFHr49HiXon1D2O3Pgilr65ncI',
    range: `4TH MASTER ROSTER!A2:E4`
  };

  let res = await gsapi.spreadsheets.values.get(options);
  let newValueArray = res.data.values;

  console.log(newValueArray);
}
