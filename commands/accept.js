const Discord = require('discord.js');

/**
 * 
 * @param {Discord.Client} client 
 * @param {Discord.Message} message 
 * @param {Array} args 
 */

exports.run = (client, message, args) => {
    console.log(`TRC_LOG_ACCEPT - START`);
    // if not in rules chan ignore
    if (message.channel.name != "rules") {
        console.log(`Command executed in wrong channel.`);
        return;
    }
    console.log(`Command executed in correct channel.`);
    
    // if user is not vistor ignore
    // TODO -- Offload to isRole(`RoleName`) util function
    if (message.member.roles.find(r => r.name == "VISITOR")) {
        console.log(`Role Found: VISITOR`);
        message.member.removeRole();
        
        // -----
        let role_selectee = message.guild.roles.find(r => r.name === "SELECTION STATUS");
        let role_interview_ready = message.guild.roles.find(r => r.name === "INTERVIEW READY");
        console.log(`Attempting role(${role_interview_ready.name}) assignment for user ${message.member.displayName}.`);
        
        // ----
        message.member.addRole(role_interview_ready).catch(console.error);
        message.member.removeRole(message.member.roles.find(r => r.name == "VISITOR"))
        // TODO - Ping S1 role.
        // TODO - ^radd @user should add a user to the roster spreadsheet.
        message.reply(`Thank you ${message.member.displayName}! A recruiter will be with you as soon as possible!`).then(message => message.delete(1000))
        message.delete(1000);
    } else {
        console.log(`Role Not Found: Vistor`);
    }

    // else good accept
        // set role selectee
        // set role interview ready
        // remove role vistor

    console.log(`TRC_LOG_ACCEPT - END`);
}