module.exports = (client, message) => {
    // Ignore all bots
    if (message.author.bot) return;
    console.log("Message Inbound!");
  
    // TODO -- Offload tracability of inbound messages as commands to a util func
    console.log(`Msg Content: ${message.content}`);
    
    // Conduct prefix check.
    if (message.content.indexOf(process.env.CMD_PREFIX) !== 0) return;
    console.log(`Prefix Detected!`);

    // Our standard argument/command name definition.
    const args = message.content.slice(process.env.CMD_PREFIX.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
  
    // TODO -- Offload into a preCmd utility function
    console.log(`Caller ID: ${message.member.id}`)
    // console.log(`Caller Permissions: ${message.member.permissions.toArray()}`);
    console.log(`Command Attempted: ${command}`);
    console.log(`Command Arguments: ${args}`);

    // Grab cmd, if it doesn't exist silently exit and do nothing
    const cmd = client.commands.get(command);
    if (!cmd) return;
  
    console.log(`Calling Command: ${command}`)
    // Run the command
    cmd.run(client, message, args);
  };