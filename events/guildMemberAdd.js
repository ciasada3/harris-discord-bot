const Discord = require('discord.js');

/**
 *
 * @param {Discord.Client} client
 * @param {Discord.GuildMember} member
 */
module.exports = (client, member) => {
    console.log(`New User Detected: ${member.user.id} | ${member.user.name}`);
    let role_visitor = member.guild.roles.find(r => r.name === "VISITOR");
    member.addRole(role_visitor).catch(console.error);
    member.send(`Welcome ${member.user.name}, please review and \`^accept\` our rules to be given access to the general chat channels.`);
}