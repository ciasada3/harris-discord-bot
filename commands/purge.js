// Bulk Delete Command
const Discord = require('discord.js');

/**
 * 
 * @param {Discord.Client} client 
 * @param {Discord.Message} message 
 * @param {Array} args 
 */
exports.run = (client, message, args) => {

    // If not admin.
    console.log(`Caller ID: ${message.member.id}`)
    console.log(`Caller Permissions: ${message.member.permissions.toArray()}`);
    if (!message.member.hasPermission("MANAGE_CHANNELS")) {
        // TODO -- Better logging / tracking of commands.
        console.log("Invalid Permissions: Bulk Delete");
        // message.reply("Sorry, you can't do that.");
        return;
    }

    let amount = null;
    let target = null;
    // Optional Param: Amount
    // Amount of messages to be removed.
    if (typeof args[0] !== 'undefined') {
        let _amount = parseInt(args[0]);
        if (_amount > 100) {
            amount = 99
        } else {
            amount = (_amount);
        }
    } else {
        console.log(`Amount Bad Param: ${args[0]} while calling purge. Defaulting to 100.`)
        amount = 99;
    }

    // Optional Param: Target
    // Target user who's messages to remove.
    // NOTE - Note used
    if (typeof args[1] !== 'undefined') {
        if (args[1].match(/<!@\d*>/g)) {
            target = args[1];
        } else {
            console.log(`Target Bad Param: ${args[1]} while calling purge. Ignoring param.`)
        }
    }

    console.log(`Purge Options: Amount To Delete = ${amount}, Target User = ${target}`);

    //message.channel.bulkDelete(amount + 1, false).then((message) => {
    //    message.channel.send(`Purge Complete!`).then(msg => msg.delete(1000));
    //}).catch(console.error);
}